defmodule AtM do
    @moduledoc """
    *AtM* is a simple Library to convert Arrays (Lists) to Matrices (Nested lists)
    """

    @doc """
    Converts a given array `arr` into a matrix with subarrays of length `len`.
    If the length of the given array `arr` is not divisible by `len` the method returns `:error`

    ## Examples

        iex> a = 1..9 |> Enum.to_list()
        [1, 2, 3, 4, 5, 6, 7, 8, 9]
        iex> AtM.to_matrix(a, 3)
        {:ok, [[1, 2, 3], [4, 5, 6], [7, 8, 9]]}
    """
    @spec to_matrix(list, integer) :: :error | {:ok, list}
    def to_matrix(arr, len) do
        if rem(length(arr), len) == 0 do
            to_matrix(arr, len, [])
        else
            :error
        end
    end

    defp to_matrix([], _len, buff) do
        {:ok, buff}
    end
    
    defp to_matrix(arr, len, buff) do
        tmp = Stream.take(arr, len) |> Enum.to_list()
        tail = drop(arr, len)

        to_matrix(tail, len, buff ++ [tmp])
    end

    defp drop(arr, 0) do
        arr
    end

    defp drop(arr, n) do
        [_ | tail] = arr
        drop(tail, n-1)
    end
end
