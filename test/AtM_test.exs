defmodule AtMTest do
    use ExUnit.Case
    doctest AtM

    test "nondivisible length" do
        test_arr = Enum.to_list(1..2)
        assert AtM.to_matrix(test_arr, 3) == :error
    end

    test "sublists in list" do
        test_arr = Enum.to_list(1..9)
        {:ok, res} = AtM.to_matrix(test_arr, 3)
        for sub <- res do
            assert is_list(sub) == true
        end
    end

    test "correct list length" do
        test_arr = Enum.to_list(1..9)
        {:ok, res} = AtM.to_matrix(test_arr, 3)
        assert length(res) == 3
    end

    test "correct subarray length" do
        test_arr = Enum.to_list(1..9)
        {:ok, res} = AtM.to_matrix(test_arr, 3)
        for sub <- res do
            assert length(sub) == 3
        end
    end

    test "correct result" do
        test_arr = Enum.to_list(1..9)
        {:ok, res} = AtM.to_matrix(test_arr, 3)
        correct_res = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
        assert res == correct_res
    end
end
